# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright(C) 2020-2021 Max-Planck-Society
# Author: Philipp Arras

from time import time

import ducc0.wgridder as wg
import ducc0.wgridder.experimental as wg_future
import numpy as np
import scipy.fft
from scipy.special.orthogonal import p_roots

speedoflight = 299792458.

try:
    import os
    nthreads = int(os.environ["OMP_NUM_THREADS"])
except:
    nthreads = 0

verbosity = 0

# Interface adapters
def dirty2ms_ducc(uvw, freq, dirty, pixsizex, pixsizey, epsilon, do_wgridding):
    return wg_future.dirty2vis(uvw=uvw, freq=freq, dirty=dirty, pixsize_x=pixsizex, pixsize_y=pixsizey, epsilon=epsilon, do_wgridding=do_wgridding,nthreads=nthreads,verbosity=verbosity)

def dirty2ms_sycl(uvw, freq, dirty, pixsizex, pixsizey, epsilon, do_wgridding):
    import syclducc
    return syclducc.dirty2vis(uvw=uvw, freq=freq, dirty=dirty, pixsize_x=pixsizex, pixsize_y=pixsizey, epsilon=epsilon, do_wgridding=do_wgridding, nthreads=nthreads)

def ms2dirty_ducc(uvw, freq, vis, npixx, npixy, pixsizex, pixsizey, epsilon, do_wgridding):
    return wg_future.vis2dirty(uvw=uvw, freq=freq, vis=vis, npix_x=npixx, npix_y=npixy, pixsize_x=pixsizex, pixsize_y=pixsizey, epsilon=epsilon, do_wgridding=do_wgridding,nthreads=nthreads,verbosity=verbosity)

def ms2dirty_sycl(uvw, freq, vis, npixx, npixy, pixsizex, pixsizey, epsilon, do_wgridding):
    import syclducc
    return syclducc.vis2dirty(uvw=uvw, freq=freq, vis=vis, npix_x=npixx, npix_y=npixy, pixsize_x=pixsizex, pixsize_y=pixsizey, epsilon=epsilon, do_wgridding=do_wgridding, nthreads=nthreads)
# /Interface adapters


def main():
    fov = 5
    nxdirty, nydirty = 1000, 1000
    nrow, nchan = 1000*1000*2, 1
    rng = np.random.default_rng(42)
    pixsizex = fov*np.pi/180/nxdirty
    pixsizey = fov*np.pi/180/nydirty*1.1
    f0 = 1e9
    freq = f0 + np.arange(nchan)*(f0/nchan)
    uvw = (rng.random((nrow, 3))-0.5)/(pixsizex*f0/speedoflight)
    uvw[:, 2] /= 20
    epsilon = 1e-5
    do_wgridding = False
    nvis = nrow*nchan

    dirty = rng.random((nxdirty, nydirty))

    ms0 = None
    implementations = dirty2ms_ducc, dirty2ms_sycl
    tf = np.float32
    tc = np.complex64
    for f in implementations:
        t0 = time()
        ms = f(uvw, freq, dirty.astype(tf), pixsizex, pixsizey, epsilon, do_wgridding)
        t1 = time()-t0
        print(f'Wall time {f.__name__} {t1:.2f} s ({nvis/t1:.0f} vis/s)')
        if ms0 is not None:
            acc = np.max(np.abs(ms-ms0)) / np.max(np.abs(ms0))
            print("Accuracy", acc)
            if acc > epsilon:
                raise RuntimeError
        else:
            ms0 = ms

    dirty0 = None
    implementations = ms2dirty_ducc, ms2dirty_sycl
    for f in implementations:
        t0 = time()
        dirty = f(uvw, freq, ms0, nxdirty, nydirty, pixsizex, pixsizey, epsilon, do_wgridding)
        t1 = time()-t0
        print(f'Wall time {f.__name__} {t1:.2f} s ({nvis/t1:.0f} vis/s)')
        if dirty0 is not None:
            acc = np.max(np.abs(dirty-dirty0)) / np.max(np.abs(dirty0))
            print("Accuracy", acc)
            if acc > epsilon:
                raise RuntimeError
        else:
            dirty0 = dirty


if __name__ == '__main__':
    main()
