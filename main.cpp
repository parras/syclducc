#include <complex>
#include <iostream>
#include <random>

#include "ducc0/infra/error_handling.h"
#include "ducc0/infra/mav.h"
#include "ducc0/math/fft.h"
#include "eskernel.h"

#include "CL/sycl.hpp"
using namespace cl;

// Includes related to pybind11
#include "ducc0/bindings/pybind_utils.h"
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
using namespace pybind11::literals;
namespace py = pybind11;
auto None = py::none();
// /Includes related to pybind11

using namespace std;
constexpr double speedoflight = 299792458.;


template <typename T>
void vis2dirty(const ducc0::mav<double, 2> &uvw,
    const ducc0::mav<double, 1> &freq,
    const ducc0::mav<complex<T>, 2> &vis,
    double pixsize_x, double pixsize_y, double epsilon, bool do_wgridding,
    const int nthreads,
    ducc0::mav<T, 2> &dirty) {
  MR_assert(!do_wgridding); // FIXME
  auto ofactor{2.};         // FIXME
  auto nxdirty{dirty.shape()[0]}, nydirty{dirty.shape()[1]};
  auto nx_padded{size_t(ofactor * nxdirty)},
       ny_padded{size_t(ofactor * nydirty)};
  auto nchan{freq.shape()[0]};
  auto nrow{uvw.shape()[0]};
  MR_assert(vis.shape()[0] == nrow);
  MR_assert(vis.shape()[1] == nchan);

  int supp;
  if (do_wgridding)
    supp = ceil(log10(1/epsilon*3)) + 1;
  else
    supp = ceil(log10(1/epsilon*2)) + 1;
  auto kernel {ESKernel<T>(supp)};

  size_t nwplanes{1}; // FIXME
  ducc0::mav<complex<T>, 2> loopim{{nx_padded, ny_padded}};
  ducc0::mav<T, 2>          dirty_padded{{nx_padded, ny_padded}};

  for (size_t ww = 0; ww < nwplanes; ++ww) {

    // Gridding
    size_t supp{kernel.support()};
    ducc0::mav<T, 2> gridre{{nx_padded, ny_padded}};
    ducc0::mav<T, 2> gridim{{nx_padded, ny_padded}};

////////////////////////////////////////////////////////////////////////////
    sycl::queue q{sycl::default_selector()};
    { // Device buffer scope
      MR_assert(uvw.contiguous());
      MR_assert(freq.contiguous());
      MR_assert(vis.contiguous());
      MR_assert(gridre.contiguous());
      MR_assert(gridim.contiguous());
      sycl::buffer<double, 2> bufuvw{uvw.cdata(), sycl::range<2>(nrow, 3)};
      sycl::buffer<double, 1> buffreq{freq.cdata(), sycl::range<1>(freq.size())};
      // QUESTION If replace vdata -> cdata, the computation silently fails
      sycl::buffer<complex<T>, 1> bufvis{ vis.cdata(),
                                          sycl::range<1>(nrow*nchan)};
      sycl::buffer<T, 2> bufgridre{gridre.vdata(),
                                   sycl::range<2>(nx_padded, ny_padded)};
      sycl::buffer<T, 2> bufgridim{gridim.vdata(),
                                   sycl::range<2>(nx_padded, ny_padded)};
      q.submit([&](sycl::handler &cgh){
          auto accuvw{bufuvw.get_access<sycl::access::mode::read>(cgh)};
          auto accfreq{buffreq.get_access<sycl::access::mode::read>(cgh)};
          auto accvis{bufvis.template get_access<sycl::access::mode::read>(cgh)};
          auto accgridre{bufgridre.template get_access<sycl::access::mode::read_write>(cgh)};
          auto accgridim{bufgridim.template get_access<sycl::access::mode::read_write>(cgh)};

          cgh.parallel_for(sycl::range<1>(nrow*nchan), [=](sycl::item<1> item){
            const size_t i{item.get_linear_id()};
            const size_t irow{i/nchan}, ichan{i-irow*nchan};

            const double u{accuvw[irow][0]*accfreq[ichan]/speedoflight*pixsize_x},
                         v{accuvw[irow][1]*accfreq[ichan]/speedoflight*pixsize_y};
         // double w{uvw.c(i, 2) * freq.c(j)};
            const double ratposx{fmod(u * nx_padded, nx_padded)};
            const double ratposy{fmod(v * ny_padded, ny_padded)};
            const auto xle{int(round(ratposx)) - int(supp) / 2};
            const auto yle{int(round(ratposy)) - int(supp) / 2};
            array<T,15> kernely;
            for (size_t a=0; a<supp; ++a) {
              kernely[a] = kernel.eval(a - ratposy + yle);
            }
            auto xle2 = xle % int(nx_padded);
            if (xle2<0) xle2 += int(nx_padded);
            auto yle2 = yle % int(ny_padded);
            if (yle2<0) yle2 += int(ny_padded);
            // Write into relevant patch
            for (size_t a=0, indx=xle2; a<supp; ++a, indx = (indx+1==nx_padded)? 0 : indx+1) {
              auto kernelx = kernel.eval(a - ratposx + xle);
              for (size_t b=0, indy=yle2; b<supp; ++b, indy = (indy+1==ny_padded)? 0 : indy+1) {
                sycl::atomic_ref<T, sycl::memory_order::relaxed, sycl::memory_scope::device> re(accgridre[indx][indy]);
                sycl::atomic_ref<T, sycl::memory_order::relaxed, sycl::memory_scope::device> im(accgridim[indx][indy]);
                re += (accvis[i]*kernelx*kernely[b]).real();
                im += (accvis[i]*kernelx*kernely[b]).imag();
              }
            }
            // /Accumulate from relevant patch

        });  // /parallel_for
      }); // /submit
    } // /Device buffer scope
////////////////////////////////////////////////////////////////////////////

    // FIXME The grid is now allocated twice
    ducc0::mav<complex<T>, 2> grid{{nx_padded, ny_padded}};
    for (size_t i=0; i<nx_padded; ++i){
      for (size_t j=0; j<ny_padded; ++j){
        const complex<T> val{gridre.c(i, j), gridim.c(i, j)};
        grid.v(i, j) = val;
      }
    }

    // FFT
    {
      ducc0::fmav<complex<T>> fgrid{grid};
      ducc0::fmav<complex<T>> floopim{loopim};
      ducc0::c2c(fgrid, floopim, {0, 1}, false, T(1.), nthreads);
    }
    // End FFT

    // FFTShift
    for (size_t i = 0; i < nx_padded; ++i) {
      for (size_t j = 0; j < ny_padded; ++j) {
        dirty_padded.v(i, j) += loopim.c((i + nx_padded / 2) % nx_padded,
                                         (j + ny_padded / 2) % ny_padded).real();
      }
    }
    // /FFTShift
  } // /wplanes


  // Kernel correction
  auto kx = vector<double>(nx_padded);
  for (size_t i=0; i<nx_padded; ++i){
    kx[i] = -0.5 + double(i)/double(nx_padded);
  }
  auto kernel_correction_x{kernel.corfunc(kx)};
  auto ky = vector<double>(ny_padded);
  for (size_t i=0; i<ny_padded; ++i){
    ky[i] = -0.5 + double(i)/double(ny_padded);
  }
  auto kernel_correction_y{kernel.corfunc(ky)};
  MR_assert(ofactor == 2); // FIXME
  MR_assert(dirty.shape()[0] % 2 == 0);
  MR_assert(dirty.shape()[1] % 2 == 0);
  for (size_t i = 0; i < nxdirty; ++i) {
    for (size_t j = 0; j < nydirty; ++j) {
      size_t bigi{nxdirty / 2 + i};
      size_t bigj{nydirty / 2 + j};
      dirty.v(i, j) = dirty_padded.c(bigi, bigj)
        / kernel_correction_x[bigi] / kernel_correction_y[bigj];
    }
  }
  // /Kernel correction

  // return dirty
}

template <typename T>
void dirty2vis(const ducc0::mav<double, 2> &uvw,
               const ducc0::mav<double, 1> &freq,
               const ducc0::mav<T, 2> &dirty, double pixsize_x,
               double pixsize_y, double epsilon, bool do_wgridding,
               const int nthreads,
               ducc0::mav<complex<T>, 2> &vis) {
  MR_assert(!do_wgridding); // FIXME
  auto ofactor{2.};         // FIXME
  auto nxdirty{dirty.shape()[0]}, nydirty{dirty.shape()[1]};
  auto nx_padded{size_t(ofactor * nxdirty)},
      ny_padded{size_t(ofactor * nydirty)};
  auto nchan{freq.shape()[0]};
  auto nrow{uvw.shape()[0]};
  MR_assert(vis.shape()[0] == nrow);
  MR_assert(vis.shape()[1] == nchan);

  int supp;
  if (do_wgridding)
    supp = ceil(log10(1/epsilon*3)) + 1;
  else
    supp = ceil(log10(1/epsilon*2)) + 1;
  auto kernel {ESKernel<T>(supp)};

  auto kx = vector<double>(nx_padded);
  for (size_t i=0; i<nx_padded; ++i){
    kx[i] = -0.5 + double(i)/double(nx_padded);
  }
  auto kernel_correction_x{kernel.corfunc(kx)};
  auto ky = vector<double>(ny_padded);
  for (size_t i=0; i<ny_padded; ++i){
    ky[i] = -0.5 + double(i)/double(ny_padded);
  }
  auto kernel_correction_y{kernel.corfunc(ky)};

  MR_assert(ofactor == 2); // FIXME
  MR_assert(dirty.shape()[0] % 2 == 0);
  MR_assert(dirty.shape()[1] % 2 == 0);
  ducc0::mav<T, 2> dirty_padded{{nx_padded, ny_padded}};
  for (size_t i = 0; i < nxdirty; ++i) {
    for (size_t j = 0; j < nydirty; ++j) {
      size_t bigi{nxdirty / 2 + i};
      size_t bigj{nydirty / 2 + j};
      dirty_padded.v(bigi, bigj) = dirty.c(i, j)
        / kernel_correction_x[bigi] / kernel_correction_y[bigj];
    }
  }

  size_t nwplanes{1}; // FIXME
  for (size_t ww = 0; ww < nwplanes; ++ww) {
    ducc0::mav<complex<T>, 2> loopim{dirty_padded.shape()};
    for (size_t i = 0; i < nx_padded; ++i) {
      for (size_t j = 0; j < ny_padded; ++j) {
        loopim.v(i, j) = dirty_padded.c((i + nx_padded / 2) % nx_padded,
                                        (j + ny_padded / 2) % ny_padded);
      }
    }

    // FFT
    ducc0::mav<complex<T>, 2> grid{dirty_padded.shape()};
    {
      ducc0::fmav<complex<T>> fgrid{grid};
      ducc0::fmav<complex<T>> floopim{loopim};
      ducc0::c2c(floopim, fgrid, {0, 1}, true, T(1.), nthreads);
    }
    // End FFT

    //  Degridding
    size_t supp{kernel.support()};
////////////////////////////////////////////////////////////////////////////

    sycl::queue q{sycl::default_selector()};
    { // Device buffer scope
      MR_assert(uvw.contiguous());
      MR_assert(freq.contiguous());
      MR_assert(vis.contiguous());
      MR_assert(grid.contiguous());
      sycl::buffer<double, 2> bufuvw{uvw.cdata(), sycl::range<2>(nrow, 3)};
      sycl::buffer<double, 1> buffreq{freq.cdata(), sycl::range<1>(freq.size())};
      // QUESTION If replace vdata -> cdata, the computation silently fails
      sycl::buffer<complex<T>, 1> bufvis{ vis.vdata(),
                                          sycl::range<1>(nrow*nchan)};
      sycl::buffer<complex<T>, 2> bufgrid{grid.cdata(),
                                          sycl::range<2>(nx_padded, ny_padded)};
      q.submit([&](sycl::handler &cgh){
          auto accuvw{bufuvw.get_access<sycl::access::mode::read>(cgh)};
          auto accfreq{buffreq.get_access<sycl::access::mode::read>(cgh)};
          auto accvis{bufvis.template get_access<sycl::access::mode::write>(cgh)};
          auto accgrid{bufgrid.template get_access<sycl::access::mode::read>(cgh)};

          cgh.parallel_for(sycl::range<1>(nrow*nchan), [=](sycl::item<1> item){
            const size_t i{item.get_linear_id()};
            const size_t irow{i/nchan}, ichan{i-irow*nchan};
            const double u{accuvw[irow][0]*accfreq[ichan]/speedoflight*pixsize_x},
                         v{accuvw[irow][1]*accfreq[ichan]/speedoflight*pixsize_y};
         // double w{uvw.c(i, 2) * freq.c(j)};
            const double ratposx{fmod(u * nx_padded, nx_padded)};
            const double ratposy{fmod(v * ny_padded, ny_padded)};
            const auto xle{int(round(ratposx)) - int(supp) / 2};
            const auto yle{int(round(ratposy)) - int(supp) / 2};
            array<T,15> kernely;
            for (size_t a=0; a<supp; ++a) {
              kernely[a] = kernel.eval(a - ratposy + yle);
            }
            complex<T> tmp=0;
            auto xle2 = xle % int(nx_padded);
            if (xle2<0) xle2 += int(nx_padded);
            auto yle2 = yle % int(ny_padded);
            if (yle2<0) yle2 += int(ny_padded);
            // Accumulate from relevant patch
            for (size_t a=0, indx=xle2; a<supp; ++a, indx = (indx+1==nx_padded)? 0 : indx+1) {
              T kernelx = kernel.eval(a - ratposx + xle);
              for (size_t b=0, indy=yle2; b<supp; ++b, indy = (indy+1==ny_padded)? 0 : indy+1) {
                tmp += accgrid[indx][indy] * kernelx*kernely[b];
              }
            }
            accvis[i] = tmp;
            // /Accumulate from relevant patch

        });  // /parallel_for
      }); // /submit
    } // /Device buffer scope
////////////////////////////////////////////////////////////////////////////
  }
}


template <typename T>
py::array Py_dirty2vis(const py::array &uvw_, const py::array &freq_,
    const py::array &dirty_, double pixsize_x, double pixsize_y, double epsilon,
    bool do_wgridding, int nthreads, py::object &vis_=None)
  {
    auto uvw = ducc0::to_mav<double,2>(uvw_, false);
    auto freq = ducc0::to_mav<double,1>(freq_, false);
    auto dirty = ducc0::to_mav<T,2>(dirty_, false);
    auto vis = ducc0::get_optional_Pyarr<complex<T>>(vis_, {uvw.shape(0),freq.shape(0)});
    auto vis2 = ducc0::to_mav<complex<T>,2>(vis, true);
    {
    py::gil_scoped_release release;
    dirty2vis<T>(uvw,freq,dirty,pixsize_x,pixsize_y,epsilon,do_wgridding,nthreads,vis2);
    }
    return move(vis);
  }


template <typename T>
py::array Py_vis2dirty(const py::array &uvw_,
    const py::array &freq_, const py::array &vis_, size_t npix_x, size_t npix_y,
    double pixsize_x, double pixsize_y, double epsilon, bool do_wgridding,
    int nthreads,
    py::object &dirty_)
  {
  auto uvw = ducc0::to_mav<double,2>(uvw_, false);
  auto freq = ducc0::to_mav<double,1>(freq_, false);
  auto vis = ducc0::to_mav<complex<T>,2>(vis_, false);
  // sizes must be either both zero or both nonzero
  MR_assert((npix_x==0)==(npix_y==0), "inconsistent dirty image dimensions");
  auto dirty = (npix_x==0) ? ducc0::get_Pyarr<T>(dirty_, 2)
                             : ducc0::get_optional_Pyarr<T>(dirty_, {npix_x, npix_y});
  auto dirty2 = ducc0::to_mav<T,2>(dirty, true);
  {
  py::gil_scoped_release release;
  vis2dirty<T>(uvw,freq,vis,pixsize_x,pixsize_y,epsilon,do_wgridding,nthreads,dirty2);
  }
  return move(dirty);
  }


PYBIND11_MODULE(syclducc, m) {
  m.def("dirty2vis", &Py_dirty2vis<float>, "FIXME insert docstring here", py::kw_only(), "uvw"_a, "freq"_a, "dirty"_a,
    "pixsize_x"_a, "pixsize_y"_a, "epsilon"_a, "do_wgridding"_a=false, "nthreads"_a, "vis"_a=None);
  m.def("vis2dirty", &Py_vis2dirty<float>, "FIXME insert docstring here", py::kw_only(), "uvw"_a, "freq"_a, "vis"_a, "npix_x"_a, "npix_y"_a,
      "pixsize_x"_a, "pixsize_y"_a, "epsilon"_a, "do_wgridding"_a=false, "nthreads"_a, "dirty"_a=None);
}
