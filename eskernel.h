using namespace std;
#include "ducc0/math/gl_integrator.h"
#include "CL/sycl.hpp"
using namespace cl;

template <typename T>
class ESKernel {
  private:
    size_t supp;
    T beta;

    T krnfunc(const T x) const {
      return sycl::exp(beta*(sycl::sqrt((T(1)-x)*(T(1)+x)) - T(1)));
    }
  public:
    ESKernel(size_t supp_):supp(supp_), beta(T(supp_*2.3)){}

    size_t support() {return supp;}

    T eval(T x) const {
      x = x/supp*2;
      if (abs(x) > 1.){
        return 0.;
      }
      return krnfunc(x);
    }

    vector<double> corfunc(const vector<double> &x) const {
      size_t nroots{2*supp};
      if (supp % 2 == 0)
        ++nroots;
      auto gl{ducc0::GL_Integrator(nroots)};
      auto q{gl.coords()};
      auto weights{gl.weights()};
      vector<double> res(x.size());
      for (size_t rr=0; rr<x.size(); ++rr){
        for (size_t i=0; i<q.size(); ++i){
          if (q[i] <= 0)
            continue;
          auto corx{x[rr]*ducc0::pi*supp};
          auto kq{corx*q[i]};
          res[rr] += supp*weights[i]*krnfunc(q[i])*cos(kq);
        }
      }
      return res;
    }
};

