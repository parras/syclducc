.PHONY: nvidia omp gcc install_on_rome install_on_valparaiso

CXXFLAGS_MINI:= -std=c++20 -Ipybind11/include -Iducc/src -IhipSYCL/install/include -pthread -Wfatal-errors -Wall -Wextra -Wno-unused-parameter -Wno-unused-local-typedef -Wno-deprecated-copy -shared -fPIC $(shell python3 -m pybind11 --includes)
CXXFLAGS:= -O3 -march=native -ffast-math $(CXXFLAGS_MINI)

SRC:= ducc/src/ducc0/infra/threading.cc main.cpp
COMMON:= $(CXXFLAGS) -o syclducc$(shell python3-config --extension-suffix) $(SRC)

nvidia : $(SRC)
	hipSYCL/install/bin/syclcc --hipsycl-targets="cuda:sm_70" $(COMMON)

omp : $(SRC)
	hipSYCL/install/bin/syclcc --hipsycl-targets="omp" --hipsycl-cpu-cxx=clang++ -fopenmp $(COMMON)

playground : playground.cpp
	hipSYCL/install/bin/syclcc --hipsycl-targets="omp" -IhipSYCL/install/include -Iducc/src -O3 -o playground playground.cpp
	./playground

gcc : $(SRC)
	g++ -fopenmp -LhipSYCL/install/lib -lhipSYCL-rt $(COMMON)

install_on_rome :
	cd hipSYCL && git clean -xfd
	mkdir hipSYCL/build
	cd hipSYCL/build && cmake -DCMAKE_CXX_COMPILER=/usr/bin/clang++-11 -DCLANG_EXECUTABLE_PATH=/usr/bin/clang++-11 -DLLVM_DIR=/usr/lib/llvm-11/cmake -DWITH_CUDA_BACKEND=ON -DWITH_ROCM_BACKEND=OFF -DWITH_LEVEL_ZERO_BACKEND=OFF -DCMAKE_INSTALL_PREFIX=../install -DCUDA_TOOLKIT_ROOT_DIR=/opt/hipSYCL/cuda ..
	cd hipSYCL/build && make install -j

install_on_tum :
	cd hipSYCL && git clean -xfd
	mkdir hipSYCL/build
	cd hipSYCL/build && cmake -DCMAKE_CXX_COMPILER=/usr/bin/clang++-12 -DCLANG_EXECUTABLE_PATH=/usr/bin/clang++-12 -DLLVM_DIR=/usr/lib/llvm-12/cmake -DWITH_CUDA_BACKEND=ON -DWITH_ROCM_BACKEND=OFF -DWITH_LEVEL_ZERO_BACKEND=OFF -DCMAKE_INSTALL_PREFIX=../install -DCUDA_TOOLKIT_ROOT_DIR=/opt/hipSYCL/cuda ..
	cd hipSYCL/build && make install -j

install_on_valparaiso :
	cd hipSYCL && git clean -xfd
	mkdir hipSYCL/build
	cd hipSYCL/build && cmake -DCMAKE_INSTALL_PREFIX=../install ..
	cd hipSYCL/build && make install -j

